import obswebsocket, obswebsocket.requests
import serial
import time
import websocket

# Scene names in OBS.
scenes = ["Ending NC", "BRB NC", "Arduino", "Game", "Starting NC", "Ending", "BRB", "DIY", "Code", "Starting", "Facecam", ]

# Requests tied to specific buttons.
requests = [
  obswebsocket.requests.SetCurrentScene(scenes[0]),
  obswebsocket.requests.SetCurrentScene(scenes[1]),
  obswebsocket.requests.SetCurrentScene(scenes[2]),
  obswebsocket.requests.SetCurrentScene(scenes[3]),
  obswebsocket.requests.SetCurrentScene(scenes[4]),
  obswebsocket.requests.SetCurrentScene(scenes[5]),
  obswebsocket.requests.SetCurrentScene(scenes[6]),
  obswebsocket.requests.SetCurrentScene(scenes[7]),
  obswebsocket.requests.SetCurrentScene(scenes[8]),
  obswebsocket.requests.SetCurrentScene(scenes[9]),
  obswebsocket.requests.SetCurrentScene(scenes[10]),
  obswebsocket.requests.SetMute("Desktop Audio", False),
  obswebsocket.requests.SetMute("Desktop Audio", True),
  obswebsocket.requests.SetMute("Mic/Aux", False),
  obswebsocket.requests.SetMute("Mic/Aux", True),
  ]

# The IP/hostname for the OBS websocket server.
# This should be localhost or 127.0.0.1 unless the daemon and OBS are on different machines.
obs_ip_address = "localhost"

# The port the websocket is listening on.
# This can be configured in the OBS Websocket settings.


# The serial port connected to the Arduino.
# Change this to whichever port is listed under Tools >> Port in the Arduino IDE.
serial_port = '/dev/ttyUSB0'

baudrate = 9600

def read_serial_and_send_websocket(client):
  try:
    with serial.Serial(serial_port, baudrate, timeout=1) as ser:
       print("Connection to serial successful")
       while True:
         # read line from serial
         inp = str(ser.readline().decode("utf-8"))
         print(inp)
         if len(inp) >= 2:
           button = int(inp[0], 16)
           state = int(inp[1], 16)

           # if button is pressed and there is corresponding request
           if state == 1 and len(requests) > button:
             client.call(requests[button])

       ser.close()
  except serial.SerialException as e:
    print(e)
  time.sleep(5)
  print("Checking serial port again")


def main():
  client = obswebsocket.obsws(obs_ip_address, 4444)
  while True:
    try:
      client.connect()
      print("Connection to OBS successful")
      while True:
        read_serial_and_send_websocket(client)
      client.disconnect()
    except websocket._exceptions.WebSocketConnectionClosedException as e:
      print("Websocket connection closed")
    except obswebsocket.exceptions.ConnectionFailure as e:
      print("Websocket connection failed")
    time.sleep(5)
    print("trying to reconnect to OBS")


if __name__ == "__main__":
  main()
