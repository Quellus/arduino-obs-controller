int inputs[] = {2,3,4,5,6};
int inputSize = 5;
int outputs[] = {7,8,9};
int outputSize = 3;

int vals[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

void setup() {
  for (int out = 0; out < outputSize; out++) {
    pinMode(outputs[out], OUTPUT);
    digitalWrite(outputs[out], LOW);
  }
  
  for (int in = 0; in < inputSize; in++) {
    pinMode(inputs[in], INPUT);
  }

  Serial.begin(9600);
  Serial.setTimeout(10);
}

void loop() {
  for (int out = 0; out < outputSize; out++) {
    digitalWrite(outputs[out], HIGH);
    for (int in = 0; in < inputSize; in++) {
      int val = digitalRead(inputs[in]);
      int valIndex = (out * inputSize) + in;
      if (val != vals[valIndex]) {
        Serial.print(valIndex, HEX);
        Serial.println(val, HEX);
        vals[valIndex] = val;
      }
      
    }
    digitalWrite(outputs[out], LOW);
  }
}
