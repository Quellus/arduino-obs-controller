# Arduino-OBS-Controller

Everything you need to DIY a controller for OBS using an Arduino
This project was created for and during live streams by [Quellus](twitch.tv/quellus).

## Dependencies
This project requires:

* An arduino
* [obs-websocket](https://github.com/Palakis/obs-websocket/)
* [pyserial](https://pythonhosted.org/pyserial/pyserial.html)
* [obs-websocket-py](https://github.com/Elektordi/obs-websocket-py)

## Arduino
I created a thing that has a lot of buttons on it, then soldered each button to individual input pins on an Arduino Nano

#### Buttons/Switches
The inputs are in PULLUP mode, so each input reads HIGH unless the switch is closed (pressed) and then it will read LOW.

The Arduino code loops through each input, storing the most recent state and checking for state changes.

#### Serial Communication
The Arduino sends information to the Python code via serial.
When the state of an input changes, the serial reads a line with the button's index and the state of the button.
The state can either be 1 indicating the button was pressed, or 0 indicating the button was released.

## Python
The Python code is a daemon. It is designed to run in the background and send commands to OBS via the OBS Websocket plugin.

### Serial
The Python code silently listens to the Arduino's serial port and does nothing until it receives certain messages.

The first character in each line of the serial messages is the index of the button, the second is the state either 1 or 0 (pressed or released).

If running on Windows, the file location for the serial port must be changed in the code. Please see the documentation for pyserial for more information (it might be something like "COM3").

### OBS Websocket
The Python code uses a websocket wrapper library designed to interface well with the OBS Websocket plugin.
The index of the button is used as the index for the request type in the "requests" array.
When that button is pressed the corresponding request is sent over the websocket.

### Daemon
There's probably ways to run the daemon automatically in the background on Windows. Let me know if you find any.
**The program should not take a lot of resources, but I make no promises on the efficiency of running the python continuously in the background**
If running on Linux, you can use Systemd to automatically start the daemon in the background when the computer starts.

1. Change the user, group, and path in `obs-arduino-deamon.service`
2. Copy the `obs-arduino-daemon.service` file to `/etc/systemd/system/`. `cp obs-arduino-daemon.service /etc/systemd/system/`
3. Run `systemctl daemon-reload` and `systemctl enable obs-arduino-daemon.service`.
4. To check if the service is running at any time, run `systemctl status obs-arduino-daemon.service`.

## License
Please see [LICENSE](LICENSE) for information on licensing.

